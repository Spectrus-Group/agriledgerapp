import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import {AzureApiSubscriptionKey,marketApi,Magic,Version} from "../../app/api";
import 'rxjs/add/operator/map';
import { IrecommendedExcercise } from '../../interfaces/recommendedExcersise.interface';
import {ImarketUserInfo} from "../../interfaces/marketUserInfo.interface";
import {Storage} from '@ionic/storage';

@Injectable()
export class MarketServiceProvider {
  headers=new Headers();
  credential={username:null,signature:null};
  userInfo={} as ImarketUserInfo;
  credentialNameForStorage='market-credential';// if you are changing this name then change this name from logout function too
  credentialLoadedFromStorage:boolean=false;
  constructor(public http: Http,private storage:Storage) {
      this.storage.get(this.credentialNameForStorage).then((credentail)=>{
        try{
          let data=JSON.parse(credentail);
          console.log('credential from localstorage is');
          console.log(data)
          if(data.username && data.signature){
            this.credential.username=data.username;
            this.credential.signature=data.signature;
          }
          this.credentialLoadedFromStorage=true;
        }
        catch (err){
          this.credentialLoadedFromStorage=false;
          console.log(err);
        }
      }).catch((err)=>{
        console.log(err);
      })

  }


  getTradingArea(){
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });

    return new Promise((resolve,reject)=>{
      this.http.post(`${marketApi.getTradingArea.url()}?roseOrder=0&category=NPC`,null,{ headers: headers }).map(res=>res.json()).subscribe((data)=>
      {
        console.log(data)
        if(data && data.error_code===0){
          for(let list of data.currencyVos){
            list.currency_img='https://market.acchain.org'+list.currency_img;
          }
          console.log(data)

          resolve(data.currencyVos);

        }
        else
          resolve([]);
      },((err)=>{

        reject(err);
      }))
    })
  }

  recommededExcercise():Promise<IrecommendedExcercise[]>{

    return new Promise((resolve,reject)=>{
      this.http.post(marketApi.recommededExcercise.url(),{}).map(res=>res.json()).subscribe((data)=>
      {
        console.log(data)
        if(data && data.error_code===0){
          for(let list of data.assetsVoList){
              list.asset_image='https://market.acchain.org/'+list.asset_image;
              }
              console.log(data)

          resolve(data.assetsVoList);

        }
        else
        resolve([]);
      },((err)=>{

        reject(err);
        }))
    })




  }

  currencyTrading(){

  }

  getUserInfo():Promise<ImarketUserInfo>{
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });

    if(!this.credential.signature)
      return Promise.reject({});
    return new Promise((resolve,reject)=>{
      this.http.post(`${marketApi.getUserInfo.url()}?username=${this.credential.username}&signature=${this.credential.signature}`,null,{ headers: headers }).map(res=>res.json()).subscribe((data)=>
      {
        console.log(data);
        if(data && data.error_code===0){
          this.userInfo=data.data;
          this.userInfo.imgUrl='https://market.acchain.org/'+this.userInfo.imgUrl;
          resolve(data.data);

        }
        else if(data && data.error_code){
          if(data.error_code===35){
            //invalid signature..login again
            this.reset();
          }
          reject({status:data.error_code})
        }
        else
          reject({});
      },((err)=>{

        reject(err);
      }))
    })
  }


reset() {
  this.credential.username = null;
  this.credential.signature=null;
  this.storage.remove(this.credentialNameForStorage).then(()=>{

  }).catch((err)=>{
    console.log(err);
  })

}

  getSignature(){
    return this.credential.signature;
  }

  setSignatureAndUsername(signature:string,username:string){
    this.credential.signature=signature;
    this.credential.username=username;
    this.storage.set(this.credentialNameForStorage,JSON.stringify({username:username,signature:signature})).then(()=>{

    }).catch((err)=>{
      console.log(err);
    })
  }

  setUsername(username:string){
    this.credential.username=username;
  }

  isCredentialLoadedFromStorage(){
    return this.credentialLoadedFromStorage;
  }

}
