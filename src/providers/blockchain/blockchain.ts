import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {AzureApiSubscriptionKey,BlockChainApi,Magic,Version} from "../../app/api";
import { Storage } from '@ionic/storage';
import {Events} from "ionic-angular";


@Injectable()
export class BlockchainProvider {

  myAccount:any={};
  headers=new Headers();
  transactions:any=[];


  constructor(public http: Http,private storage:Storage,public events:Events) {
    this.headers.append('version',Version);
    this.headers.append('magic',Magic);
    this.subscribeEvents();

  }


  private resetAllData(){
    this.transactions=[];
    this.myAccount={};
  }

  /*all get services */

  public getTransactions(publicKey:string,address:string){
    let headers=new Headers();
    headers.append('Ocp-Apim-Subscription-Key',AzureApiSubscriptionKey);
    headers.append('Content-Type','application/json')

    if(this.transactions.length){
      console.log('returning txn from cache')
      return Promise.resolve(this.transactions);
    }
    console.log('returning txn from server')

    return new Promise((resolve,reject)=>{

      this.http.get(`${BlockChainApi.transactionsApi.url()}/?senderPublicKey=${publicKey}&recipientId=${address}&orderBy=t_timestamp:desc`,{headers:headers})
        .subscribe((data)=>{
          this.transactions=data.json();
          resolve(data.json())
        },(err)=>{

          reject(err)
        })
    })
  }

  public getAccountFromBlockchain(address:string){

    if(this.myAccount.account)
    {
      console.log('returning account from cache')
      return Promise.resolve(this.myAccount);
    }
    return new Promise((resolve,reject)=>{
      this.http.get(BlockChainApi.accountApi.url()+'?address='+address)
        .subscribe((response)=>{
          this.myAccount=response.json();
          resolve(response.json())
        },(err)=>{

          reject(err)
        })
    })
  }

  public getMyBalance(address:string){
    return new Promise((resolve,reject)=>{
      this.http.get(`${BlockChainApi.myBalancesApi.url()}/${address}`,{headers:this.headers})
        .subscribe((response)=>{
          let data=response.json();
          console.log('uia balance are')

          console.log(data);
          if(data && !data.success){
            resolve([]);
          }
          for (let i in data.balances) {
            let precision = data.balances[i].precision;
            data.balances[i].balance = parseInt(data.balances[i].balance) / Math.pow(10, precision);
            data.balances[i].quantity = parseInt(data.balances[i].quantity) / Math.pow(10, precision);
          }

          resolve(data.balances)
        },(err)=>{

          reject(err)
        })
    })
  }

  public getIssuer(address:string){
    return new Promise((resolve,reject)=>{
      this.http.get(`${BlockChainApi.issuerApi.url()}/${address}`,{headers:this.headers})
        .subscribe((response)=>{
          let data=response.json();
          if(data && !data.success)
            resolve({});
          else
            resolve(data.issuer)
        },(err)=>{

          reject(err)
        })
    })
  }

  public getChainAssets(){
    return new Promise((resolve,reject)=>{
      this.http.get(`${BlockChainApi.myAssetsApi.url()}/__SYSTEM__/assets`,{headers:this.headers})
        .subscribe((response)=>{
          let data=response.json();
          if(data && !data.success)
            resolve([]);

          for (let i in data.assets) {
            let precision = data.assets[i].precision;
            data.assets[i].maximum = parseInt(data.assets[i].maximum) / Math.pow(10, precision);
            data.assets[i].quantity = parseInt(data.assets[i].quantity) / Math.pow(10, precision);
          }

          resolve(data.assets);


        },(err)=>{

          reject(err)
        })
    })
  }


  public getMyAssets(publisherName:string){
    if(!publisherName)
      return Promise.resolve([]);
    return new Promise((resolve,reject)=>{
      this.http.get(`${BlockChainApi.myAssetsApi.url()}/${publisherName}/assets`,{headers:this.headers})
        .subscribe((response)=>{
          let data=response.json();

          if(data && !data.success)
            resolve([]);
          for (let i in data.assets) {
            let precision = data.assets[i].precision;
            data.assets[i].maximum = parseInt(data.assets[i].maximum) / Math.pow(10, precision);
            data.assets[i].quantity = parseInt(data.assets[i].quantity) / Math.pow(10, precision);
          }

          resolve(data.assets);


        },(err)=>{

          reject(err)
        })
    })
  }

  public getAssetCategoryList(level:number){
    return new Promise((resolve,reject)=>{
      this.http.get(`${BlockChainApi.assetcategorynextApi.url()}/${level}`,{headers:this.headers})
        .subscribe((response)=>{
          let data=response.json();
          if(data && !data.success)
            resolve([]);
          else
            resolve(data.categories)
        },(err)=>{

          reject(err)
        })
    })
  }


  public getUserIssuedAssetBalance(){

  }
  /*
  all post services
   */
  public createAccount(profile:any){
    return new Promise((resolve,reject)=>{
       this.http.post(BlockChainApi.loginApi.url(),{publicKey:profile.publicKey},{headers:this.headers})
       .subscribe((response)=>{
          resolve(response.json())
        },(err)=>{
          reject(err)
        })
    })
  }


  public createTransaction(txn:any){
    return new Promise((resolve,reject)=>{
      this.http.post(BlockChainApi.postApi.url(),{transaction:txn},{headers:this.headers})
        .subscribe((response)=>{
          let data=response.json();
          if(!data.success){
            reject(data.error)
          }
          resolve(data)
        },(err)=>{

          reject(err)
        })
    })
  }


/*

Events
 */
  private subscribeEvents(){
    this.events.subscribe('pullToRefreshCalled',(data)=>{
      console.log('pull to refresh has been called');
      this.resetAllData();
    })
  }

}
