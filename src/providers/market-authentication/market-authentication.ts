import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import {AzureApiSubscriptionKey,marketApi,Magic,Version} from "../../app/api";
import 'rxjs/add/operator/map';
import {MarketServiceProvider} from "../market-service/market-service";

@Injectable()
export class MarketAuthenticationProvider {

  constructor(public http: Http,private marketService:MarketServiceProvider) {
    console.log('Hello MarketAuthenticationProvider Provider');
  }


  login(credential:any):Promise<{username:string,signature:string,mobile:string,error_code:number}>{
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });

    return new Promise((resolve,reject)=>{
      this.http.post(`${marketApi.login.url()}?username=${credential.username}&password=${credential.password}`,null,{ headers: headers }).map(res=>res.json()).subscribe((data)=>
      {
        console.log(data);
        if(data && data.error_code===0){
          this.marketService.setSignatureAndUsername(data.signature,data.username);
          resolve(data);
        }
        else
          reject(data.error_code);
      },((err)=>{

        reject(err);
      }))
    })

  }
}
