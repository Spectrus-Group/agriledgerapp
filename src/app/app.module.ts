import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import {HttpModule, Http} from '@angular/http';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { WelcomePage } from '../pages/welcome/welcome';
import {AlertPage} from "../pages/alert/alert";
import {UploadPage} from "../pages/upload/upload";
import {SpinnerDialog} from "@ionic-native/spinner-dialog";
import {FileUploadModule} from "ng2-file-upload/index";
import { IonicStorageModule } from '@ionic/storage';
import { BlockchainProvider } from '../providers/blockchain/blockchain';
import {ProfilePage} from "../pages/profile/profile";
import {PipesModule} from "../pipes/pipes.module";
import {LedgerPage} from "../pages/ledger/ledger";
import { LoopbackProfileProvider } from '../providers/loopback-profile/loopback-profile';
import {LedgerDetailsPage} from "../pages/ledger-details/ledger-details";
import {WalletPage} from "../pages/wallet/wallet";
import {CoinTransferPage} from "../pages/coin-transfer/coin-transfer";
import { ToastProvider } from '../providers/toast/toast';
import { SpinnerProvider } from '../providers/spinner/spinner';
import {AssetsPage} from "../pages/assets/assets";
import {AssetIssuePage} from "../pages/asset-issue/asset-issue";
import {AssetIssuedPage} from "../pages/asset-issued/asset-issued";
import {AssetChainPage} from "../pages/asset-chain/asset-chain";
import {IssuerRegistrationPage} from "../pages/issuer-registration/issuer-registration";
import { UploadProvider } from '../providers/upload/upload';
import {ComponentsModule} from "../components/components.module";
import {ComingsoonComponent} from "../components/comingsoon/comingsoon";
import {InformationPage} from "../pages/information/information";
import {MarketPage} from "../pages/market/market";
import {PlanningPage} from "../pages/planning/planning";
import {ClipboardModule} from "ngx-clipboard/dist/src";
import {LoginPage} from "../pages/login/login";
import {PostRegisterPage} from "../pages/post-register/post-register";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {File} from '@ionic-native/file';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { FingerprintProvider } from '../providers/fingerprint/fingerprint';
import {CommunityPage} from "../pages/community/community";
import { PinDialog } from '@ionic-native/pin-dialog';
import { PinDialogProvider } from '../providers/pin-dialog/pin-dialog';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { TranslateServiceProvider } from '../providers/translate-service/translate-service';
import {TokenIssuePage} from "../pages/token-issue/token-issue";
import { MarketServiceProvider } from '../providers/market-service/market-service';
import { HeaderPage } from '../pages/header/header';
import {MarketLoginPage} from "../pages/market-login/market-login";
import {MarketRegisterPage} from "../pages/market-register/market-register";
import { MarketAuthenticationProvider } from '../providers/market-authentication/market-authentication';
import {SignUpPage} from "../pages/sign-up/sign-up";

export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http,'./assets/translate/', '.json');
}


@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    WelcomePage,
    AlertPage,
    UploadPage,
    ProfilePage,
    WalletPage,
    CoinTransferPage,
    LedgerPage,
    LedgerDetailsPage,
    AssetsPage,
    AssetIssuePage,
    AssetIssuedPage,
    AssetChainPage,
    IssuerRegistrationPage,
    InformationPage,
    MarketPage,
    PlanningPage,
    LoginPage,
    PostRegisterPage,
    CommunityPage,
    TokenIssuePage,
    HeaderPage,
    MarketLoginPage,
    MarketRegisterPage,
    SignUpPage
  ],
  imports: [
    ComponentsModule,
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    PipesModule,
    FileUploadModule,
    ClipboardModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: ['sqlite','indexeddb', 'websql']
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    WelcomePage,
    AlertPage,
    UploadPage,
    ProfilePage,
    WalletPage,
    CoinTransferPage,
    LedgerPage,
    LedgerDetailsPage,
    AssetsPage,
    AssetIssuePage,
    AssetIssuedPage,
    AssetChainPage,
    IssuerRegistrationPage,
    ComingsoonComponent,
    InformationPage,
    MarketPage,
    PlanningPage,
    LoginPage,
    PostRegisterPage,
    CommunityPage,
    TokenIssuePage,
    HeaderPage,
    MarketLoginPage,
    MarketRegisterPage,
    SignUpPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SpinnerDialog,
    BlockchainProvider,
    LoopbackProfileProvider,
    ToastProvider,
    SpinnerProvider,
    UploadProvider,
    File,
    FingerprintAIO,
    PinDialog,
    FingerprintProvider,
    PinDialogProvider,
    TranslateServiceProvider,
    MarketServiceProvider,
    MarketAuthenticationProvider
  ]
})
export class AppModule {}
