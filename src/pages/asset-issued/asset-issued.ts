import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {TokenIssuePage} from "../token-issue/token-issue";


@Component({
  selector: 'page-asset-issued',
  templateUrl: 'asset-issued.html',
})
export class AssetIssuedPage {

  myAssets=[];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.myAssets=this.navParams.get('myAssets')||[];
    console.log(this.myAssets)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AssetIssuedPage');
  }

  gotoTokenIssuePage(asset:any){

    this.navCtrl.push(TokenIssuePage,{asset:asset});
  }
}
