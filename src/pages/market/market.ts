import { Component } from '@angular/core';
import {NavController, NavParams, LoadingController, Events} from 'ionic-angular';
import {WelcomePage} from "../welcome/welcome";
import {ToastProvider} from "../../providers/toast/toast";
import {LoopbackProfileProvider} from "../../providers/loopback-profile/loopback-profile";
import { MarketServiceProvider } from '../../providers/market-service/market-service';
import { IrecommendedExcercise } from '../../interfaces/recommendedExcersise.interface';
import {MarketLoginPage} from "../market-login/market-login";
import {ImarketUserInfo} from "../../interfaces/marketUserInfo.interface";


@Component({
  selector: 'page-market',
  templateUrl: 'market.html',
})
export class MarketPage {

  recommendedExcercise:IrecommendedExcercise[]=[];
  tradingArea=[];
  loggedIn:boolean=false;
  userInfo={} as ImarketUserInfo;
  getUserInfoPromise:string='resolved';
  getRecommendedExcercisePromise:string='resolved';
  getTradingAreaPromise:string='resolved';

  constructor(public navCtrl: NavController,
              private marketService:MarketServiceProvider,private events:Events,
              public navParams: NavParams,private loopbackService:LoopbackProfileProvider,
              private toastProvider:ToastProvider,
              private loadingCtrl:LoadingController) {

    this.loggedIn=this.marketService.getSignature() ? true : false;
    console.log('is logged in ', this.loggedIn);
    this.events.subscribe('market-loggedIn',(user:{username:string,signature:string})=>{
      this.getUserInfo();
      this.loggedIn=true;
      console.log('event subscribed')
      console.log(this.loggedIn)
    })
  }

  ionViewDidLoad() {
    console.log('ion view did load')
    this.getUserInfo();
    this.getrecommededExcercise();
    this.getTradingArea();
  }


  async getUserInfo(){
    try{
      this.getUserInfoPromise='pending';
      this.userInfo=await  this.marketService.getUserInfo();
      console.log(this.userInfo);
      this.getUserInfoPromise='resolved';

      return this.userInfo;

    }
    catch (err){
      this.getUserInfoPromise='rejected';

      if(err&& err.status===35){
        this.loggedIn=false;
        this.userInfo={};
        this.toastProvider.presentToast('Signature is expired.please login again to access trading','bottom',5000);
      }
      console.log(err);
      return null;
    }
  }
  async getrecommededExcercise():Promise<IrecommendedExcercise[]>{

    try{
      this.getRecommendedExcercisePromise='pending';

      let data:IrecommendedExcercise[]=await this.marketService.recommededExcercise();
      this.recommendedExcercise=data;
      console.log(this.recommendedExcercise);
      this.getRecommendedExcercisePromise='resolved';

      return this.recommendedExcercise;
    }
    catch(err){
      this.getRecommendedExcercisePromise='rejected';

      console.log(err);
      return null;

    }

  }

  async getTradingArea():Promise<any>{

    try{
      this.getTradingAreaPromise='pending';
      let data:any=await this.marketService.getTradingArea();
      this.tradingArea=data;
      console.log(this.tradingArea);
      this.getTradingAreaPromise='resolved';

      return this.tradingArea;
    }
    catch(err){
      this.getTradingAreaPromise='rejected';

      console.log(err);
      return null;

    }

  }


  private async doRefresh(refresher){
    refresher.complete();

    try{
      await  this.getrecommededExcercise();
      await this.getUserInfo();

    }
    catch (err){

    }



  }




  goToMarketLoginPage(){
    this.navCtrl.push(MarketLoginPage);
  }
}
