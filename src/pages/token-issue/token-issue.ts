import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {LoopbackProfileProvider} from "../../providers/loopback-profile/loopback-profile";
import {Iprofile} from "../../interfaces/profile.interface";
import {BlockchainProvider} from "../../providers/blockchain/blockchain";
import {ToastProvider} from "../../providers/toast/toast";

declare var agrichainJS;

@Component({
  selector: 'page-token-issue',
  templateUrl: 'token-issue.html',
})
export class TokenIssuePage {
  profile:Iprofile={address:'',privateKey:'',publicKey:'',name:'',location:'',secondPublicKey:''};

  myAsset:any;
  exchangeRate:number|string=null;
  amount:any=null;
  tokenIssuePromise:string='resolved';
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private blockChainProvider:BlockchainProvider,
              private toastService:ToastProvider,
              private loopbackProfileProvider:LoopbackProfileProvider)
  {
    this.myAsset=this.navParams.get('asset')||{};
    console.log(this.myAsset);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TokenIssuePage');
    this.loopbackProfileProvider.getProfile().then((profile:any)=> {
      this.profile = profile;
  }).catch((err)=>{

    })

  }


  issueToken(){
    if(!this.amount){
      this.toastService.presentToast('Invalid amount');
      return;
    }

    if(!this.exchangeRate){
      this.toastService.presentToast('Invalid exchangeRate');
      return;

    }
    let realAmount:any = parseInt(this.amount) * Math.pow(10, this.myAsset.precision);

    let exchangeRate = this.exchangeRate.toString();
    let currency=this.myAsset.currency;

    let trs = agrichainJS.uia.createIssue(currency, String(realAmount), exchangeRate,this.profile.privateKey,this.profile.secondPublicKey);
    this.tokenIssuePromise='pending';
    this.blockChainProvider.createTransaction(trs)
      .then((data)=>{
        this.exchangeRate=null;
        this.amount=null;
        this.tokenIssuePromise='resolved';
        this.toastService.presentToast('You have issued your token.wait until it gets approved','bottom',5000);

      })

      .catch((err)=>{
        this.tokenIssuePromise='rejected';
        this.toastService.presentToast(err||'Something went wrong');

    })
  }
}
