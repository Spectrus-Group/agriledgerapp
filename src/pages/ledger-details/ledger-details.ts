import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LedgerDetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-ledger-details',
  templateUrl: 'ledger-details.html',
})
export class LedgerDetailsPage {

  transaction:any={};

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.transaction=this.navParams.get('txn')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LedgerDetailsPage');

    console.log(this.transaction)
  }


}
