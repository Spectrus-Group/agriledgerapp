import { Component } from '@angular/core';
import {NavController, NavParams, Events, LoadingController} from 'ionic-angular';
import {CoinTransferPage} from "../coin-transfer/coin-transfer";
import {BlockchainProvider} from "../../providers/blockchain/blockchain";
import {LoopbackProfileProvider} from "../../providers/loopback-profile/loopback-profile";
import {Iprofile} from "../../interfaces/profile.interface";
import {ToastProvider} from "../../providers/toast/toast";
import {AssetsPage} from "../assets/assets";
import {WelcomePage} from "../welcome/welcome";



@Component({
  selector: 'page-wallet',
  templateUrl: 'wallet.html',
})
export class WalletPage {

  myAccount:any={account:{}};
  profile:Iprofile={address:'',privateKey:'',publicKey:'',name:'',location:''};

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private loadingCtrl:LoadingController,
              private blockchainProvider:BlockchainProvider,
              public events:Events,
              private loopbackService:LoopbackProfileProvider,
              public toastProvider:ToastProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WalletPage');

    this.loopbackService.getProfile().then((profile:any)=>{
      this.profile=profile;
      return this.blockchainProvider.getAccountFromBlockchain(profile.address)
    }).then((account:any)=>{
      this.myAccount=account;

    }).catch((err)=>{
      this.toastProvider.presentToast('Something went wrong');

    })
  }

   async cashTransfer(){

    this.navCtrl.push(CoinTransferPage);


  /*   this.fingerprintProvider.isFingerPrintAvailable().then((isAvailable:boolean)=>{

     }).catch((err)=>{

       this.pinService.show().then((pin:any)=>{

       }).catch((err)=>{

       })
     });*/

  }



  doRefresh(refresher){
    this.events.publish('pullToRefreshCalled');
    setTimeout(()=>{
      this.blockchainProvider.getAccountFromBlockchain(this.profile.address).then((account:any)=>{
        this.myAccount = account;
        refresher.complete();

      }).catch((err)=>{
        this.toastProvider.presentToast('Something went wrong');
        refresher.complete();

      })
    },0);


  }

  private goToAssetsPage(){
    this.navCtrl.push(AssetsPage);
  }
  logout(){
    let loader = this.loadingCtrl.create({
      content: "Logging you out",
      spinner: 'crescent'
    });

    loader.present();


    this.loopbackService.logout().then(()=>{
      loader.dismiss();
      this.navCtrl.parent.parent.setRoot(WelcomePage);
    }).catch((err)=>{
      loader.dismiss();
      this.toastProvider.presentToast(err);
    })
  }
}
