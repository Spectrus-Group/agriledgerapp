import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ModalController, Events, ToastController} from 'ionic-angular';
import {Http, Headers} from "@angular/http";
import {BlockchainProvider} from "../../providers/blockchain/blockchain";
import {ProfileApi, AzureApiSubscriptionKey} from "../../app/api";
import {LoopbackProfileProvider} from "../../providers/loopback-profile/loopback-profile";
import {Iprofile} from "../../interfaces/profile.interface";
import {ToastProvider} from "../../providers/toast/toast";
import {FingerprintProvider} from "../../providers/fingerprint/fingerprint";
import {PinDialogProvider} from "../../providers/pin-dialog/pin-dialog";
import { ActionSheetController } from 'ionic-angular';

declare var agrichainJS;
@Component({
  selector: 'page-coin-transfer',
  templateUrl: 'coin-transfer.html',
})
export class CoinTransferPage {

  profile:Iprofile={address:'',privateKey:'',publicKey:'',name:'',location:''};
  profilePromise = 'pending';
  txnCreatePromise='resolved';
  transaction:any={currency:'ACC',precision:6,fee:'0.01',toAddress:'',amount:''};
  currencyRange:Array<any> = [];
  constructor(public navCtrl:NavController,
              public navParams:NavParams,
              public http:Http,
              public modalCtrl:ModalController,
              private events:Events,
              private pinService:PinDialogProvider,
              private fingerprintProvider:FingerprintProvider,
              public actionSheetCtrl: ActionSheetController,

              private blockchainProvider:BlockchainProvider,
              public toastProvider:ToastProvider,
              private loopbackProfileProvider:LoopbackProfileProvider)
  {

  }



  ionViewDidLoad() {
    this.loopbackProfileProvider.getProfile().then((profile:Iprofile)=>{
      this.profile = profile;
      this.profilePromise = 'resolved';
      return this.blockchainProvider.getMyBalance(profile.address);


    }).then((balance:any)=>{
        this.currencyRange = balance;
        this.currencyRange.unshift({
          currency: "ACC",
          precision: 6
        });

      })
      .catch((err)=> {
        this.profilePromise = 'rejected';
        console.log(err)
        this.toastProvider.presentToast('Something went wrong');

      })



  }

/*

   presentActionSheetWithFingerprint(transaction:any) {

    let actionSheet = this.actionSheetCtrl.create({
      title: 'Authenticate using',
      buttons: [
        {
          text: '6 digit Passcode',
          icon:'person',
          handler: () => {
            this.fingerprintProvider.passcodeVerfication().then((passcode:string)=>{

              if(passcode==''+this.profile.passcode){
                this.sendTransaction(transaction);

              }
              else{
                this.toastProvider.presentToast('Invalid passcode');
              }
            })
          }
        },
        {
          text: 'Fingerprint',
          handler: () => {
            this.fingerprintProvider.fingerprintLogin().then((data)=>{
              this.sendTransaction(transaction);
            }).catch((err)=>{
              console.log(err);
            })
          }
        }
        ,{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  presentActionSheetWithoutFingerprint(transaction:any) {

    let actionSheet = this.actionSheetCtrl.create({
      title: 'Authenticate using',
      buttons: [
        {
          text: '6 digit Passcode',
          icon:'person',
          handler: () => {
            this.fingerprintProvider.passcodeVerfication().then((passcode:string)=>{

              if(passcode==''+this.profile.passcode){
                this.sendTransaction(transaction);

              }
              else{
                this.toastProvider.presentToast('Invalid passcode');
              }
            })
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }*/

  private getPrecision(currency){
    for( let i=0;i<this.currencyRange.length;i++){
      if(this.currencyRange[i].currency == currency){
        return this.currencyRange[i].precision;
      }
    }
  }
  private async createTransaction(){
    var isAddress = /^[0-9]{1,21}$/g;
    var currency;
    var transaction;
    var precision =6;
    precision = this.transaction.currency === 'ACC' ? 6 : this.getPrecision(this.transaction.currency);

    console.log(precision)
    if(this.transaction.currency === undefined){
      this.toastProvider.presentToast('Invalid Amount');
    }else{
      currency = this.transaction.currency === 'ACC' ? null : this.transaction.currency;

    }
    if (!this.transaction.toAddress) {
      this.toastProvider.presentToast('Please enter recipient address');
      return false;
    }
    // if (!isAddress.test($scope.fromto)) {
    //     toastError($translate.instant('ERR_RECIPIENT_ADDRESS_FORMAT'));
    //     return false;
    // }
    if (this.transaction.toAddress === this.profile.address) {
      this.toastProvider.presentToast('Please choose different address other than yours');
      return false;
    }
    if (!this.transaction.amount || Number(this.transaction.amount) <= 0) {
      this.toastProvider.presentToast('Invalid Amount');
      return false;
    }
    var amount = parseFloat((this.transaction.amount * Math.pow(10,precision)).toFixed(0));
    if (!this.profile.secondPublicKey) {
      this.profile.secondPublicKey = '';
    }
    console.log(amount,currency)
    transaction = agrichainJS.transaction.createTransaction(String(this.transaction.toAddress), amount.toString(), currency, this.transaction.remark,this.profile.privateKey, this.profile.secondPublicKey);

    console.log(transaction);

    this.fingerprintProvider.presentActionSheet(this.sendTransaction,this,this.profile.passcode,transaction);
 


  }


  async sendTransaction(transaction:any){
    try{
      this.txnCreatePromise='pending';
      let txn:any=await this.blockchainProvider.createTransaction(transaction);
      this.txnCreatePromise='resolved';
      if(txn.success){
        this.toastProvider.presentToast('Sent');
        this.transaction.amount='';
        this.transaction.toAddress='';
      }
      else{
        this.toastProvider.presentToast(txn.error||'Something went wrong');
      }

    }
    catch (err){
      this.txnCreatePromise='resolved';
      console.log(err);
      this.toastProvider.presentToast(err);

    }
  }



  onCurrencyChange(e){
    console.log(e);
  }


}
