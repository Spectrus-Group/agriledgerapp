import { Component } from '@angular/core';
import {NavController, NavParams, Events, LoadingController} from 'ionic-angular';
import {LoopbackProfileProvider} from "../../providers/loopback-profile/loopback-profile";
import {BlockchainProvider} from "../../providers/blockchain/blockchain";
import {ToastProvider} from "../../providers/toast/toast";
import {AssetIssuePage} from "../asset-issue/asset-issue";
import {AssetIssuedPage} from "../asset-issued/asset-issued";
import {AssetChainPage} from "../asset-chain/asset-chain";
import {IssuerRegistrationPage} from "../issuer-registration/issuer-registration";
import {Iprofile} from "../../interfaces/profile.interface";
import {CoinTransferPage} from "../coin-transfer/coin-transfer";
import {WelcomePage} from "../welcome/welcome";

@Component({
  selector: 'page-assets',
  templateUrl: 'assets.html',
})
export class AssetsPage {
  pet='kittens';
  myAccount:any={};
  profile:Iprofile={address:'',privateKey:'',publicKey:'',name:'',location:''};
  assetIssuer:any={};
  isIssuerRegistered:boolean=false;
  chainAssets=[];
  myAssets=[];
  uiaBalance=[];

  issuerPromise='pending';
  uiaBalancePromise='pending';
  constructor(private navCtrl: NavController,
              private loadingCtrl:LoadingController,
              private navParams: NavParams,
              private loopbackService:LoopbackProfileProvider,
              private blockchainProvider:BlockchainProvider,
              private toastProvider:ToastProvider,
              private events:Events) {

    this.events.subscribe('issuer-registered',()=>{
      console.log('publisher register event fired')
      this.isIssuerRegistered=true;
    })

    this.events.subscribe('asset-registered',()=>{
      console.log('asset register event fired');
    })


  }


  ionViewDidLoad() {

    this.loopbackService.getProfile().then((profile:any)=>{
      this.profile=profile;
      return this.blockchainProvider.getAccountFromBlockchain(profile.address)
    }).then((account:any)=>{
      this.myAccount=account;
      return this.blockchainProvider.getIssuer(this.profile.address);
    })
      .then((issuer:any)=>{
        this.issuerPromise='resolved'
        this.assetIssuer=issuer;
        console.log('issuer is')
        console.log(issuer)
        if(issuer.name)
          this.isIssuerRegistered=true;
        else
          this.isIssuerRegistered=false;

        return this.blockchainProvider.getMyAssets(this.assetIssuer.name);

      })
      .then((myAssets:any)=>{
      console.log('myAssets')
      console.log(myAssets)
      this.myAssets=myAssets;
      return this.blockchainProvider.getChainAssets();

    })
      .then((chainAssets:any)=>{
        console.log('chain aseets')
        console.log(chainAssets)
        this.chainAssets=chainAssets;

        return this.blockchainProvider.getMyBalance(this.profile.address);

      })
      .then((balance:Array<any>)=>{
        this.uiaBalancePromise='resolved';
        this.uiaBalance=balance;
      })

      .catch((err)=>{
        this.issuerPromise='rejected';
        this.toastProvider.presentToast('Something went wrong');

    })
  }


  goToIssuerRegisterPage(){
    this.navCtrl.push(IssuerRegistrationPage)

  }
  goToAssetIssuePage(){
    this.navCtrl.push(AssetIssuePage)
  }

  goToAssetIssuedPage(){
    this.navCtrl.push(AssetIssuedPage,{myAssets:this.myAssets})

  }
  goToAssetChainPage(){
    this.navCtrl.push(AssetChainPage,{chainAssets:this.chainAssets})

  }

  goToTransferPage(){
    this.navCtrl.push(CoinTransferPage);

  }

  logout(){
    let loader = this.loadingCtrl.create({
      content: "Logging you out",
      spinner: 'crescent'
    });

    loader.present();


    this.loopbackService.logout().then(()=>{
      loader.dismiss();
      this.navCtrl.parent.parent.setRoot(WelcomePage);
    }).catch((err)=>{
      loader.dismiss();
      this.toastProvider.presentToast(err);
    })
  }
}
