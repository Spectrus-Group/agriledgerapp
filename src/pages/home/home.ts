import { Component } from '@angular/core';
import {NavController, LoadingController} from 'ionic-angular';
import { Http } from '@angular/http';
import {BlockchainProvider} from "../../providers/blockchain/blockchain";
import {WalletPage} from "../wallet/wallet";
import {WelcomePage} from "../welcome/welcome";
import {ToastProvider} from "../../providers/toast/toast";
import {LoopbackProfileProvider} from "../../providers/loopback-profile/loopback-profile";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,
              public http:Http,
              private toastProvider:ToastProvider,
              private loadingCtrl:LoadingController,
              private loopbackService:LoopbackProfileProvider,
              private blockchainProvider:BlockchainProvider) {

  }


  ionViewDidLoad() {

  }

  goToWalletPage(){
    this.navCtrl.push(WalletPage);
  }

  goToProfileTab(){

    this.navCtrl.parent.select(5)
  }

  goToMarketTab(){
    this.navCtrl.parent.select(2)

  }

  goToLedgerTab(){
    this.navCtrl.parent.select(3)

  }

  logout(){
    let loader = this.loadingCtrl.create({
      content: "Logging you out",
      spinner: 'crescent'
    });

    loader.present();

    this.loopbackService.logout().then(()=>{
      loader.dismiss();
      this.navCtrl.parent.parent.setRoot(WelcomePage);
    }).catch((err)=>{
      loader.dismiss();
      this.toastProvider.presentToast(err);
    })
  }
}
