import {Component} from '@angular/core';
import {NavController, NavParams, Events} from 'ionic-angular';
import {LoopbackProfileProvider} from "../../providers/loopback-profile/loopback-profile";
import {Iprofile} from "../../interfaces/profile.interface";
import {BlockchainProvider} from "../../providers/blockchain/blockchain";
import {ToastProvider} from "../../providers/toast/toast";

declare var agrichainJS;

@Component({
  selector: 'page-asset-issue',
  templateUrl: 'asset-issue.html',
})
export class AssetIssuePage {

  profile:Iprofile = {address: '', privateKey: '', publicKey: '', name: '', location: '', secondPublicKey: ''};

  registerPromise = 'resolved';
  lastCategoryId = '';
  asset:any = {
    name: '',
    currency: '',
    estimatedPrice: '',
    estimatedUnit: '',
    unlockCondition: '',
    excerciseStandard: '',
    chainOrNot: false,
    description: '',
    category: {level1: {}, level2: {}, level3: {}, level4: {}, level5: {}},
    publishedNumber: {maximum: '', precision: ''}
  };

  chosenLang = 1;
  currencyUnits = ['USD', 'RMB', 'JPY', 'EUR', 'GBP'];

  unlockCondition = [{name: 'Community vote unlock', value: '0'}, {name: 'ICO contrcat unlock', value: '1'}];
  assetCategoriesLevelOne = [];
  assetCategoriesLevelTwo = [];
  assetCategoriesLevelThree = [];
  assetCategoriesLevelFour = [];
  assetCategoriesLevelFive = [];

  deepestCategorySelected = false;

  constructor(public navCtrl:NavController,
              public navParams:NavParams,
              public loopbackProfileProvider:LoopbackProfileProvider,
              public blockChainProvider:BlockchainProvider,
              public toastProvider:ToastProvider,
              public events:Events) {
  }


  ionViewDidLoad() {
    this.loopbackProfileProvider.getProfile().then((profile:any)=> {
      this.profile = profile;

      return this.blockChainProvider.getAssetCategoryList(0);
    })
      .then((assetCategory:Array<any>)=> {
        this.assetCategoriesLevelOne = assetCategory;
        console.log(this.assetCategoriesLevelOne)
      })
      .catch((err)=> {
        this.toastProvider.presentToast('Something went wrong');

      });
    console.log('ionViewDidLoad IssuerRegistrationPage');
  }

  onAssetCategoryLevelChange(category:any, level:number) {
    console.log(category)
    console.log(level)

    if (!category.hasChildren) {
      this.deepestCategorySelected = true;
      this.lastCategoryId = category.id;
      return;
    }
    return this.blockChainProvider.getAssetCategoryList(category.id)
      .then((assetCategory:Array<any>)=> {
        console.log(assetCategory)
        if (level == 1) {
          this.assetCategoriesLevelTwo = assetCategory;
        }
        if (level == 2) {
          this.assetCategoriesLevelThree = assetCategory;
        }

        if (level == 3) {
          this.assetCategoriesLevelFour = assetCategory;
        }

        if (level == 4) {
          this.assetCategoriesLevelFive = assetCategory;
        }

      })
      .catch((err)=> {
        this.toastProvider.presentToast('Something went wrong');

      });
  }

  registerAsset() {

    if (!this.asset.name) {
      return this.toastProvider.presentToast('Asset name required');
    } else {
      var strLen = this.asset.name.replace(/[^\x00-\xff]/g, "**").length;
      if (strLen > 256) {
        return this.toastProvider.presentToast('Asset name should not be more than 256 in length');
      }
    }
    console.log(this.asset.currency)
    if (!this.asset.currency) {
      return this.toastProvider.presentToast('Asset currency required');
    } else {
      var reg = /^[A-Z0-9][A-Z0-9]{2,9}$/;
      if (!reg.test(this.asset.currency)) {
        return this.toastProvider.presentToast('Asset currency invalid. If it is chainLevel then BTC,ETH otherwise PublisherName.CurrencyName'
          , 'bottom', 8000);
      }
    }
    if (!this.asset.estimatedPrice) {
      return this.toastProvider.presentToast('Estimate price is required');
    } else {
      var reg = /^(([1-9]|[1-9]\d{0,8})|(([1-9]|[1-9]\d{0,8})\.[0-9][0-9]{0,1})|(0\.(0[1-9]|[1-9][0-9]{0,1})))$/g;
      if (!reg.test(this.asset.estimatedPrice)) {
        return this.toastProvider.presentToast('Estimate price is invalid');
      }
    }

    if (!this.asset.excerciseStandard) {
      return this.toastProvider.presentToast('Exercise standard is required');
    } else {
      var reg = /^[1-9][0-9]{0,8}$/;
      if (!reg.test(this.asset.excerciseStandard)) {
        return this.toastProvider.presentToast('Exercise standard is invalid');
      }
    }

    if (!this.asset.description) {
      return this.toastProvider.presentToast('Asset description is required');
    } else {
      var strLen = this.asset.description.replace(/[^\x00-\xff]/g, "**").length;
      if (strLen > 4096) {
        return this.toastProvider.presentToast('Asset description should not be more than 4096 in length');
      }
    }

    if (!this.asset.estimatedUnit) {
      return this.toastProvider.presentToast('Estimate unit is required');
    }

    if (!this.asset.unlockCondition) {
      return this.toastProvider.presentToast('Unlock condition is required');
    }

    if (!parseInt(this.asset.publishedNumber.maximum)) {
      return this.toastProvider.presentToast('Invalid maximum amout of publisher');
    }
    if (!/^[0-6]$/.test(this.asset.publishedNumber.precision)) {
      return this.toastProvider.presentToast('Accuracy should me maximum 6 in length');
    }


    if (!this.asset.category.level1.id) {
      return this.toastProvider.presentToast('Asset category level one is required');
    }
    if (!this.deepestCategorySelected) {
      return this.toastProvider.presentToast('Choose next asset category');
    }


    let name = this.asset.name;
    let currency = this.asset.chainOrNot ? this.asset.currency : this.asset.name + '.' + this.asset.currency;
    let estimatePrice = this.asset.estimatedPrice;
    let estimateUnit = this.asset.estimatedUnit;
    let unlockCondition = Number(this.asset.unlockCondition);
    let exerciseUnit = this.asset.excerciseStandard;
    let desc = this.asset.description;
    let maximum = this.asset.publishedNumber.maximum;
    let precision = Number(this.asset.publishedNumber.precision);
    let realMaximum = parseInt(maximum) * Math.pow(10, precision);


    var extra = JSON.stringify({
      "productBrand": {
        "value": '',
        "remark": '',
        "link": ''
      },
      "packingStandard": {
        "value": '',
        "remark": '',
        "link": ''
      },
      "productIndex": {
        "value": '',
        "remark": "",
        "link": ""
      },
      "productionInfo": {
        "value": "",
        "remark": "",
        "link": ""
      },
      "otherInfo": {
        "value": "",
        "remark": "",
        "link": ""
      },
      "moreDetails": ""
    })
    var payload = {
      name: name,
      currency: currency,
      desc: desc,
      category: this.lastCategoryId,
      precision: precision,
      maximum: realMaximum.toString(),
      estimateUnit: estimateUnit,
      estimatePrice: estimatePrice.toString(),
      exerciseUnit: exerciseUnit.toString(),
      unlockCondition: unlockCondition,
      extra: extra
    }
    const assetTrs = agrichainJS.uia.createAsset(payload, this.profile.privateKey, this.profile.secondPublicKey);

    this.registerPromise = 'pending';
    this.blockChainProvider.createTransaction(assetTrs).then((data)=> {
      console.log(data);
      this.registerPromise = 'resolved';
      this.toastProvider.presentToast('Asset has been created');
      this.asset = {
        name: '',
        currency: '',
        estimatedPrice: '',
        estimatedUnit: '',
        unlockCondition: '',
        excerciseStandard: '',
        chainOrNot: false,
        description: '',
        category: {level1: {}, level2: {}, level3: {}, level4: {}, level5: {}},
        publishedNumber: {maximum: '', precision: ''}
      };

      this.events.publish('asset-registered');
    }).catch((err)=> {
      console.log(err);
      this.registerPromise = 'rejected';

      this.toastProvider.presentToast(err || 'Asset could not be created.try again!');
    })
  }
}
