import {Component, ViewChild} from '@angular/core';
import { HomePage } from '../home/home';
import {ProfilePage} from "../profile/profile";
import {LedgerPage} from "../ledger/ledger";
import {InformationPage} from "../information/information";
import {MarketPage} from "../market/market";
import {PlanningPage} from "../planning/planning";
import {AssetsPage} from "../assets/assets";
import {CommunityPage} from "../community/community";
import {WalletPage} from "../wallet/wallet";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AssetsPage;
  tab3Root = MarketPage;
  tab4Root = CommunityPage;
  tab5Root = WalletPage;
  tab6Root = ProfilePage;


  constructor() {

  }

  ionViewDidEnter() {
/*
    this.tabRef.select(5);
*/
  }
}
