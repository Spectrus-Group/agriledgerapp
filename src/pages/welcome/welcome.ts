import {TabsPage} from './../tabs/tabs';
import {Component} from '@angular/core';
import {NavController, LoadingController, AlertController} from 'ionic-angular';
import 'rxjs/add/operator/toPromise';
import {Storage} from '@ionic/storage';
import {LoopbackProfileProvider} from "../../providers/loopback-profile/loopback-profile";
import {BlockchainProvider} from "../../providers/blockchain/blockchain";
import {Iprofile} from "../../interfaces/profile.interface";
import {ToastProvider} from "../../providers/toast/toast";
import {PostRegisterPage} from "../post-register/post-register"
import {changeURL} from '../../app/api'
import {TranslateServiceProvider} from "../../providers/translate-service/translate-service";
import {SignUpPage} from "../sign-up/sign-up";


declare var Mnemonic, agrichainJS;
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  profile:Iprofile = {
    id: null,
    address: null,
    privateKey: null,
    publicKey: null,
    name: null,
    location: null,
    passcode: null
  };

  lastTappedTimestamp:number = 0;
  totalTapCount:number = 0;
  defaultLangauge:string = 'ch';
  showdropdown:boolean = false;
  msg1 = 'Agriledger, Revolutionising the Supply Chain';
  msg2 = 'Creating opportunities across the value chain';
  msg3 = 'Guaranteeing Authenticity and Quantity';
  msg4 = 'Creating Transparency and Traceability';

  constructor(public navCtrl:NavController,
              private storage:Storage,
              private alertCtrl:AlertController,
              public loadingCtrl:LoadingController,
              private loopbackProfileProvider:LoopbackProfileProvider,
              private blockchainProvider:BlockchainProvider,
              public toastProvider:ToastProvider,
              private translateService:TranslateServiceProvider) {

  }

  changeLang() {
    this.defaultLangauge = this.defaultLangauge === 'ch' ? 'en' : 'ch';
    this.translateService.changeLang(this.defaultLangauge);
    this.showdropdown = false
    console.log(this.defaultLangauge)
  }

  ionViewDidLoad() {

  }

  signIn() {
    //this.navCtrl.push(PostRegisterPage)
    //this.fingerprintLogin();
    this.showPrompt();
  }

  showPrompt() {
    let msg1 = '6 digit Passcode';
    msg1 = this.translateService.dynamicTranslation(msg1);
    let msg2 = 'passcode';
    msg2 = this.translateService.dynamicTranslation(msg2);
    let msg3 = 'Paste here';
    msg3 = this.translateService.dynamicTranslation(msg3);
    let msg4 = 'Cancel';
    msg4 = this.translateService.dynamicTranslation(msg4);
    let msg5 = 'SIGN IN';
    msg5 = this.translateService.dynamicTranslation(msg5);
    let prompt = this.alertCtrl.create({
      title: msg1,
      inputs: [
        {
          name: msg2,
          placeholder: msg3
        },
      ],
      buttons: [
        {
          text: msg4,
          handler: data => {
          }
        },
        {
          text: msg5,
          handler: login => {
            this.checkIfUserIsRegistered(login.passcode);
          }
        }
      ]
    });
    prompt.present();
  }

  showPromptTochangeURL() {
    let prompt = this.alertCtrl.create({
      title: 'Change URL',
      message: 'Format: http://ip:port',
      inputs: [
        {
          name: 'lbURL',
          placeholder: 'Loopback URL'
        },
        {
          name: 'bcURL',
          placeholder: 'Blockchain URL'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
          }
        },
        {
          text: 'CHANGE',
          handler: urls => {
            console.log(urls);
            if (!urls.lbURL && !urls.bcURL)
              return false;
            changeURL(urls.lbURL, urls.bcURL);
            this.storage.set('urlconfig', {lbURL: urls.lbURL, bcURL: urls.bcURL});
            let alert = this.alertCtrl.create({
              title: 'URL Changed!',
              subTitle: 'Now onwards this URL will be used even if you restart the app.anytime you can change it after 5 times tap on the same page',
              buttons: ['OK']
            });
            alert.present();
          }
        }
      ]
    });
    prompt.present();
  }

  checkIfUserIsRegistered(passcode:any) {
    if (!passcode || passcode.length === 0) {
      this.toastProvider.presentToast('Passcode can not be left blank');
      return;
    }

    let loader = this.loadingCtrl.create({
      content: "Please wait We are signing you in...",
      spinner: 'crescent'
    });

    loader.present();
    console.log(`passcode is ${passcode}`)
    this.loopbackProfileProvider.getProfileByPasscode(passcode).then((profile:Iprofile)=> {
      loader.dismiss();
      this.storage.set('agriId', profile.id);

      this.navCtrl.push(TabsPage);
    }).catch((err)=> {
      loader.dismiss();
      console.log(err)
      this.toastProvider.presentToast(err);

    })
  }

  randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  async skip() {
    let msg = 'Please wait We are creating account for you...';
    msg = this.translateService.dynamicTranslation(msg);
    let loader = this.loadingCtrl.create({
      content: msg,
      spinner: 'crescent'
    });

    this.profile.id = null;
    this.profile.passcode = null;
    this.profile.address = null;
    this.profile.publicKey = null;
    this.profile.privateKey = null;
    this.profile.secondPublicKey = null;

    let privateKey = new Mnemonic(Mnemonic.Words.ENGLISH).toString();
    let publicKey = agrichainJS.crypto.getKeys(privateKey).publicKey;
    let passcode = this.randomIntFromInterval(122114, 999965);
    this.profile.passcode = passcode;
    this.profile.privateKey = privateKey;
    this.profile.publicKey = publicKey;
    console.log(this.profile)
    loader.present();

    let isProfileCreated:boolean = false;
    let isBlockchainAccountCreated:boolean = false;
    let isProfileUpdated:boolean = false;
    try {
      this.profile = await this.loopbackProfileProvider.createProfile(this.profile);
      isProfileCreated = true;
    }
    catch (err) {
      console.log(err);
      this.toastProvider.presentToast(err.status === 0 ? 'Could not connect to server' : 'Something went wrong');
    }
    if (isProfileCreated) {
      try {
        let account:any = await this.blockchainProvider.createAccount(this.profile);
        this.profile.address = account.account.address;
        isBlockchainAccountCreated = true;
      }
      catch (err) {
        console.log(err);
        this.toastProvider.presentToast(err.status === 0 ? 'Could not connect to server' : 'Something went wrong');
      }
    }
    if (isBlockchainAccountCreated) {
      try {
        this.profile = await this.loopbackProfileProvider.updateProfile(this.profile);
        await this.storage.set('agriId', this.profile.id);
        loader.dismiss();
        isProfileUpdated = true;
        this.navCtrl.push(PostRegisterPage, {profile: this.profile});
        return;
      }
      catch (err) {
        console.log(err);
        this.toastProvider.presentToast(err.status === 0 ? 'Could not connect to server' : 'Something went wrong');
      }
    }
    loader.dismiss();

  }

  onTap(e) {
    if (this.lastTappedTimestamp !== 0 && e.timeStamp - this.lastTappedTimestamp < 1200) {
      this.totalTapCount++;
      this.lastTappedTimestamp = e.timeStamp;
    }
    else {
      if (this.lastTappedTimestamp === 0) {
        this.totalTapCount = 1;
        this.lastTappedTimestamp = e.timeStamp;
      }
      else {
        this.lastTappedTimestamp = 0;
        this.totalTapCount = 0;
      }
    }
    if (this.totalTapCount === 5) {
      this.totalTapCount = 0;
      this.lastTappedTimestamp = 0;
      this.showPromptTochangeURL();
    }
    console.log(this.totalTapCount);
  }

  formVal() {
    this.navCtrl.push(SignUpPage);
  }

}
