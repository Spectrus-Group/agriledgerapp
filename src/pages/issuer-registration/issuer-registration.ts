import { Component } from '@angular/core';
import {NavController, NavParams, Events} from 'ionic-angular';
import {ToastProvider} from "../../providers/toast/toast";
import {BlockchainProvider} from "../../providers/blockchain/blockchain";
import {LoopbackProfileProvider} from "../../providers/loopback-profile/loopback-profile";
import {Iprofile} from "../../interfaces/profile.interface";

declare var agrichainJS;
@Component({
  selector: 'page-issuer-registration',
  templateUrl: 'issuer-registration.html',
})
export class IssuerRegistrationPage {
  issuer={name:'',desc:''};
  isPublisherRegistered=false;
  profile:Iprofile={address:'',privateKey:'',publicKey:'',name:'',location:'',secondPublicKey:''};
  registerPromise='resolved';
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public toastProvider:ToastProvider,
              public blockChainProvider:BlockchainProvider,
              public loopbackProfileProvider:LoopbackProfileProvider,
              public events:Events) {
  }

  ionViewDidLoad() {
    this.loopbackProfileProvider.getProfile().then((profile:any)=> {
      this.profile = profile;
    })
      .catch((err)=>{
        this.toastProvider.presentToast('Something went wrong');

      });
    console.log('ionViewDidLoad IssuerRegistrationPage');
  }


  registerPublisher(){
    if(!this.profile.privateKey){
      return this.toastProvider.presentToast('We could not get your private key');

    }

    let reg = /^[A-Z]{1,16}$/;
    if(!reg.test(this.issuer.name)){
      return this.toastProvider.presentToast('Issuer name invalid.Name should be in all capital and 1-16 character long','bottom',6000);
    }

    if(!this.issuer.desc){
      return this.toastProvider.presentToast('Issuer description required');
    }
    if (!this.profile.secondPublicKey) {
      this.profile.secondPublicKey = '';
    }
    let publishTxn = agrichainJS.uia.createIssuer(this.issuer.name, this.issuer.desc, this.profile.privateKey, this.profile.secondPublicKey);

    console.log(publishTxn)
    this.registerPromise='pending';
    this.blockChainProvider.createTransaction(publishTxn).then((data)=>{
      console.log(data)
      this.isPublisherRegistered=true;
      this.registerPromise='resolved';
      this.toastProvider.presentToast('Issuer has been registered');
      this.events.publish('issuer-registered');
    }).catch((err)=>{
      console.log(err)
      this.registerPromise='rejected';

      this.toastProvider.presentToast(err||'Issuer could not be created.try again!');
    })
  }

}
