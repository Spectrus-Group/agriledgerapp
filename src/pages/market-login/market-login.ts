import { Component } from '@angular/core';
import {Events, NavController, NavParams} from 'ionic-angular';
import {MarketAuthenticationProvider} from "../../providers/market-authentication/market-authentication";

import {Md5} from 'ts-md5/dist/md5';
import {ToastProvider} from "../../providers/toast/toast";
import {MarketPage} from "../market/market";
@Component({
  selector: 'page-market-login',
  templateUrl: 'market-login.html',
})
export class MarketLoginPage {

  public loginForm: any;
  credential={username:null,password:null};
  public backgroundImage = 'assets/img/background/background-5.jpg';

  constructor(public navCtrl: NavController,private toastService:ToastProvider,
              private event:Events,
              public navParams: NavParams,private marketAuthService:MarketAuthenticationProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MarketLoginPage');
  }

  async login(){
    this.credential.password=Md5.hashStr(this.credential.password);
    console.log(this.credential)

    try{
      let user:{username:string,signature:string,mobile:string,error_code:number}= await this.marketAuthService.login(this.credential);
      console.log('login success');
      console.log(user)
      this.event.publish('market-loggedIn',user);
      this.navCtrl.pop();
    }
    catch (err){
      this.toastService.presentToast('Login failed');
      console.log(err)

    }
  }

  goToRegisterPage(){

  }
}
