import { Component } from '@angular/core';
import {NavController, NavParams, Events, LoadingController} from 'ionic-angular';
import {BlockchainProvider} from "../../providers/blockchain/blockchain";
import {LoopbackProfileProvider} from "../../providers/loopback-profile/loopback-profile";
import {LedgerDetailsPage} from "../ledger-details/ledger-details";
import {ToastProvider} from "../../providers/toast/toast";
import { Storage } from '@ionic/storage';
import {WelcomePage} from "../welcome/welcome";



@Component({
  selector: 'page-ledger',
  templateUrl: 'ledger.html',
})
export class LedgerPage {
  profile:any={name:'',agriId:'',location:'',profileUrl:'',id:undefined};
  transactions=[];
  txnPromise='pending';
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public blockchainProvider:BlockchainProvider,
              public events:Events,
              private loadingCtrl:LoadingController,private storage:Storage,
              public loopbackService:LoopbackProfileProvider,
              public toastProvider:ToastProvider)
  {

  }

  ionViewDidLoad() {
    this.loopbackService.getProfile().then((profile)=>{
      this.profile=profile;
      console.log(this.profile);
      return this.blockchainProvider.getTransactions(this.profile.publicKey,this.profile.address);
    }).then((txn:any)=>{
      this.transactions=txn.transactions;
      console.log(this.transactions);
      this.txnPromise='resolved';

    }).catch((err)=>{
      console.log(err);
      this.txnPromise='rejected';

    })
  }



  doRefresh(refresher){
    this.events.publish('pullToRefreshCalled');
    setTimeout(()=>{
      console.log('getting txn')
      this.blockchainProvider.getTransactions(this.profile.publicKey,this.profile.address).then((txn:any)=>{
        this.transactions=txn.transactions;
        refresher.complete();

      }).catch((err)=>{
        this.toastProvider.presentToast('Something went wrong');
        refresher.complete();

      })
    },0);


  }

  goToLedgerDetailsPage(txn:any){
    console.log(txn)
    this.navCtrl.push(LedgerDetailsPage,{txn:txn});
  }

  logout(){
    let loader = this.loadingCtrl.create({
      content: "Logging you out",
      spinner: 'crescent'
    });

    loader.present();
    this.loopbackService.logout().then(()=>{
      loader.dismiss();
      this.navCtrl.parent.parent.setRoot(WelcomePage);
    }).catch((err)=>{
      loader.dismiss();
      this.toastProvider.presentToast(err);
    })
  }
}
