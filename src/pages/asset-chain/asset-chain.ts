import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AssetChainPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-asset-chain',
  templateUrl: 'asset-chain.html',
})
export class AssetChainPage {
  chainAssets=[];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.chainAssets=this.navParams.get('chainAssets')||[];
    console.log(this.chainAssets)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AssetChainPage');
  }

}
