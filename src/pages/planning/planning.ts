import { Component } from '@angular/core';
import {NavController, NavParams, LoadingCmp, LoadingController} from 'ionic-angular';
import {WalletPage} from "../wallet/wallet";
import {WelcomePage} from "../welcome/welcome";
import {ToastProvider} from "../../providers/toast/toast";
import {LoopbackProfileProvider} from "../../providers/loopback-profile/loopback-profile";


@Component({
  selector: 'page-planning',
  templateUrl: 'planning.html',
})
export class PlanningPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private loopbackService:LoopbackProfileProvider,
              private loadingCtrl:LoadingController,private toastProvider:ToastProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlanningPage');
  }
  private goToWalletPage() {
    this.navCtrl.push(WalletPage);
  }

  logout(){
    let loader = this.loadingCtrl.create({
      content: "Logging you out",
      spinner: 'crescent'
    });

    loader.present();


    this.loopbackService.logout().then(()=>{
      loader.dismiss();
      this.navCtrl.parent.parent.setRoot(WelcomePage);
    }).catch((err)=>{
      loader.dismiss();
      this.toastProvider.presentToast(err);
    })
  }
}
