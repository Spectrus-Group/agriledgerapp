import {Component} from '@angular/core';
import {NavController, LoadingController, AlertController} from 'ionic-angular';
import {AzureApiSubscriptionKey, ProfileApi, ServerUrl, BlockChainApi} from '../../../src/app/api'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {ModalController} from 'ionic-angular';
import {UploadPage} from "../upload/upload";
import {IUploadPageConfig} from "../../interfaces/uploadPageConfig.interface";
import {Events} from 'ionic-angular';
import {BlockchainProvider} from "../../providers/blockchain/blockchain";
import {ToastController} from 'ionic-angular';
import {LoopbackProfileProvider} from "../../providers/loopback-profile/loopback-profile";
import {WalletPage} from "../wallet/wallet";
import {Iprofile} from "../../interfaces/profile.interface";
import {Storage} from '@ionic/storage';
import {WelcomePage} from "../welcome/welcome";
import {ToastProvider} from "../../providers/toast/toast";
import {TranslateServiceProvider} from "../../providers/translate-service/translate-service";

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  profile:Iprofile = {address: '', privateKey: '', publicKey: '', name: '', location: '', passcode: null};
  serverUrl = ServerUrl;
  myAccount:any = {};
  accountPromise = 'pending';
  profilePromise = 'pending';
  profileUpdatePromise = 'resolved';
  authenticationQueryParameter = '?subscription-key=' + AzureApiSubscriptionKey;

  constructor(public navCtrl:NavController,
              public modalCtrl:ModalController,
              private loadingCtrl:LoadingController,
              private storage:Storage,
              private events:Events,
              private toastProvider:ToastProvider,
              private blockchainProvider:BlockchainProvider,
              private loopbackService:LoopbackProfileProvider,
              public toastCtrl:ToastController, private translateService:TranslateServiceProvider,
              private alertCtrl:AlertController) {
  }


  onCopyText() {
    this.presentToast('copied to clipboard');
  }


  ionViewDidLoad() {
    this.loopbackService.getProfile().then((profile)=> {

      this.profile = profile;
      console.log(this.profile);
      this.profilePromise = 'resolved';
      return this.blockchainProvider.getAccountFromBlockchain(this.profile.address)

    }).then((account:any)=> {
      this.myAccount = account;
      console.log(this.myAccount)
      this.accountPromise = 'resolved';
    })
      .catch((err)=> {
        console.log('error is')
        console.log(err);
        if (this.profilePromise === 'pending')
          this.profilePromise = 'rejected';
        this.accountPromise = 'rejected';
        this.presentToast('Something went wrong');

      })
  }

  ionViewWillEnter() {
    console.log('willl enter')
    this.subscribeEvents();

  }

  ionViewWillLeave() {
    console.log('will leave')
    this.unsubscribeEvents();

  }


  private doRefresh(refresher) {

    this.events.publish('pullToRefreshCalled');

    setTimeout(()=> {
      this.blockchainProvider.getAccountFromBlockchain(this.profile.address).then((account:any)=> {
        this.myAccount = account;
        console.log(account)
        refresher.complete();

      }).catch((err)=> {
        this.presentToast('Something went wrong');
        refresher.complete();

      })
    }, 0);
  }

  private presentUploadModal() {
    let configToPass:IUploadPageConfig = {
      headerName: 'Upload Profile',
      uploadType: 'profile',
      id: this.profile.id
    };
    let modal = this.modalCtrl.create(UploadPage, {config: configToPass});
    modal.onDidDismiss(data => {
      console.log(data);
    });
    modal.present();
  }


  private updateProfile() {

    if (!this.profile.name || !this.profile.location)
      return false;
    this.profileUpdatePromise = 'pending';
    this.loopbackService.updateProfile(this.profile).then((profile)=> {
      this.profile = profile;
      this.profileUpdatePromise = 'resolved';

      this.presentToast('Updated')
    }).catch((err)=> {
      this.profileUpdatePromise = 'rejected';
      this.presentToast('Something went wrong')
    });

  }

  private presentToast(message:string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  private goToWalletPage() {
    this.navCtrl.push(WalletPage);
  }

  private subscribeEvents() {
    this.events.subscribe('profileImage:uploaded', (url:string)=> {
      console.log('profile uploaded event recieved')
      console.log(url)
      this.profile.profileUrl = url;
    })
  }

  private unsubscribeEvents() {
    this.events.unsubscribe('profileImage:uploaded');
  }

  logout() {
    let loader = this.loadingCtrl.create({
      content: "Logging you out",
      spinner: 'crescent'
    });

    loader.present();


    this.loopbackService.logout().then(()=> {
      loader.dismiss();
      this.navCtrl.parent.parent.setRoot(WelcomePage);
    }).catch((err)=> {
      loader.dismiss();
      this.toastProvider.presentToast(err);
    })
  }

  changeID() {
    let msg1 = '6 digit Passcode';
    msg1 = this.translateService.dynamicTranslation(msg1);
    let msg2 = 'passcode';
    msg2 = this.translateService.dynamicTranslation(msg2);
    let msg3 = 'Paste here';
    msg3 = this.translateService.dynamicTranslation(msg3);
    let msg4 = 'Cancel';
    msg4 = this.translateService.dynamicTranslation(msg4);
    let msg5 = 'SAVE';
    msg5 = this.translateService.dynamicTranslation(msg5);
    let prompt = this.alertCtrl.create({
      title: msg1,
      inputs: [
        {
          name: msg2,
          placeholder: msg3
        },
      ],
      buttons: [
        {
          text: msg4,
          handler: data => {
          }
        },
        {
          text: msg5,
          handler: login => {
            this.updatePasscode(login.passcode);
          }
        }
      ]
    });
    prompt.present();
  }

  updatePasscode(passcode:any) {
    let msg6 = 'Please wait We are updating the passcode for you...';
    msg6 = this.translateService.dynamicTranslation(msg6);
    if (!passcode || passcode.length === 0) {
      this.toastProvider.presentToast('Passcode can not be left blank');
      return;
    }

    let loader = this.loadingCtrl.create({
      content: msg6,
      spinner: 'crescent'
    });

    loader.present();
    this.profile.passcode=passcode;
    this.loopbackService.updateProfile(this.profile).then((profile:Iprofile)=> {
        loader.dismiss();
    }).catch((err)=> {
      loader.dismiss();
      console.log(err)
      this.toastProvider.presentToast(err);

    })
  }
}
