import { Component, ViewChild, trigger, transition, style, state, animate, keyframes } from '@angular/core';
import {NavController, Slides, NavParams} from 'ionic-angular';
import {File} from '@ionic-native/file';

import {TabsPage} from '../tabs/tabs'
import {Iprofile} from "../../interfaces/profile.interface";
import {ToastProvider} from "../../providers/toast/toast";

declare var cordova:any;
@Component({
  selector: 'post-register-page',
  templateUrl: 'post-register.html',
  animations: [

    trigger('bounce', [
      state('*', style({
        transform: 'translateX(0)'
      })),
      transition('* => rightSwipe', animate('700ms ease-out', keyframes([
        style({transform: 'translateX(0)', offset: 0}),
        style({transform: 'translateX(-65px)', offset: .3}),
        style({transform: 'translateX(0)', offset: 1})
      ]))),
      transition('* => leftSwipe', animate('700ms ease-out', keyframes([
        style({transform: 'translateX(0)', offset: 0}),
        style({transform: 'translateX(65px)', offset: .3}),
        style({transform: 'translateX(0)', offset: 1})
      ])))
    ])
  ]
})
export class PostRegisterPage {
  @ViewChild(Slides) slides: Slides;
  skipMsg: string = "Skip";
  state: string = 'x';
  profile:Iprofile={address:'',privateKey:'',publicKey:'',name:'',location:''};
  constructor(public navCtrl: NavController,private navParms:NavParams,private file:File,private toastProvider:ToastProvider) {

    this.profile=this.navParms.get('profile');
    console.log(this.profile)
  }

  skip() {
    this.navCtrl.setRoot(TabsPage);

  }

  saveFile(){
    this.file.writeFile(cordova.file.applicationDirectory,'agriledger.txt','sss',{replace:true}).then((data)=>{
      this.toastProvider.presentToast('Downloaded');

    }).catch((err)=>{
      this.toastProvider.presentToast(err);

    })
  }

  slideChanged() {
    if (this.slides.isEnd())
      this.skipMsg = "Alright, I got it";
  }

  slideMoved() {
    if (this.slides.getActiveIndex() >= this.slides.getPreviousIndex())
      this.state = 'rightSwipe';
    else
      this.state = 'leftSwipe';
  }

  animationDone() {
    this.state = 'x';
  }

}
